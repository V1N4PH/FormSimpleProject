FROM tomcat:latest
RUN mkdir -p /usr/local/myjava
WORKDIR /usr/local/myjava
COPY target/*.war bin.war
